public class LengthOfString {

    String string;
    LengthOfString(String string){
        this.string = string;

    }
    public int countTheCharacters(){
        int count =0;
        for(int iteration=0;iteration !=string.length();iteration++){
            if(string.charAt(iteration)==' '){
                continue;
            }
            if(Character.isAlphabetic(string.charAt(iteration))){
                count++;
            }
        }
        return count;
    }
}
