import org.junit.jupiter.api.Test;
import static  org.junit.jupiter.api.Assertions.*;
class LenghtOfStringTest {

    @Test
    void shouldReturnLengthAsFiveWhenAWhiteSpaceTreeIsGiven() {
        LengthOfString string = new LengthOfString("A Tree");
        int expected = 5;
        int actual = string.countTheCharacters();

        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnTheLengthAsTwelveWhenThoughtworksIsGiven() {
        LengthOfString string = new LengthOfString("Thoughtworks");
        int expected = 12;
        int actual = string.countTheCharacters();

        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnLengthAsFiveWhenASpecialCharacterTreeIsGiven() {
        LengthOfString string = new LengthOfString("A%Tree");
        int expected = 5;
        int actual = string.countTheCharacters();

        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnLengthAsFiveWhenANumberTreeIsGiven(){
        LengthOfString string = new LengthOfString("A%Tree");
        int expected = 5;
        int actual = string.countTheCharacters();

        assertEquals(expected,actual);


    }




}